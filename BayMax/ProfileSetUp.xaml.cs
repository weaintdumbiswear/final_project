﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace BayMax
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class ProfileSetUp : Page
    {
        /// <summary>
        /// the object to access the list of profiles
        /// </summary>
        private PeopleProfiles _personProfile;

        public ProfileSetUp()
        {
            this.InitializeComponent();
            _personProfile = null;

        }

        /// <summary>
        /// Getting the personProfile object passed by the pervious page
        /// </summary>
        /// <param name="e"></param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            _personProfile = e.Parameter as PeopleProfiles;

        }

        private async void OnCreateProfile(object sender, RoutedEventArgs e)
        {
            //Creating the profile 
            try
            {
                //setting the person properties
                string name = _txtName.Text;
                int day = int.Parse(_txtDay.Text);
                int year = int.Parse(_txtYear.Text);
                double height = double.Parse(_txtHeight.Text);
                double weight = double.Parse(_txtWeight.Text);
                int gender = GenderType.SelectedIndex;
                int diabetes = _cmbDiabetes.SelectedIndex;
                
                //saving all the properties
                Person newPerson = new Person();
                newPerson.Name = name;
                newPerson.Day = day;
                newPerson.Month = _cmbMonths.SelectedIndex+1;
                newPerson.Year = year;
                newPerson.Height = height;
                newPerson.Weight = weight;
                newPerson.TheGender = gender;
                //Calculate the BMI and zodiac sign
                newPerson.Calculate();
                newPerson.Type_diabetes = diabetes;
                //Add the new person's profile to the list
                _personProfile.AddProfile(newPerson);
                
                //let the user know it has been created
                MessageDialog msgDlg = new MessageDialog("Profile created successfully.", "New Profile");
                await msgDlg.ShowAsync();
            }
            //In case some format is not right, throw an exception
            catch (FormatException)
            {
                MessageDialog errMsgDlg = new MessageDialog("Input entered was not correct.", "Error");
                await errMsgDlg.ShowAsync();
            }
            //in case of any other error occuring
            catch (Exception ex)
            {
                MessageDialog errMsgDlg = new MessageDialog($" An error happened when creating the profile.\n Error: {ex.Message}", "Error");
                await errMsgDlg.ShowAsync();
            }
        }
    }
}
